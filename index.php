<!DOCTYPE html>
    <?php
        /**
         * require modules.
         * This also means if you made a new you do it here.
         */
        require "dependencies/lib.php";
        require "dependencies/cindex.php";
        require "dependencies/config.php";
        require "dependencies/database.php";
        require "dependencies/security.php";
        require "dependencies/controllers.php";
        require "dependencies/content.php";
        require "dependencies/views.php";
        require "dependencies/system.php";

        $system  = new system();

    ?>
<html>
    <head>
        <!-- <title><?php echo $title ?></title> -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php
            $system->getContent()->getPublicCss();
            $system->getContent()->getPublicJS();
            $system->getContent()->getPageCss();
            $system->getContent()->getPageJs();
        ?>
    </head>
    <body>
        <?php
            $system->getViews()->getPublicTopViews();
            $system->getViews()->getPageViews();
            $system->getViews()->getPublicBottomViews();
        ?>
        <footer>
        </footer>
    </body>
</html>