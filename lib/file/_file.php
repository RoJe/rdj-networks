<?php
	/**
     * name       file 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */ 
	class _file{

		public function __construct(){
		}

		/**
		 * Get the content from a single file.
		 * @param  String $path Path to file
		 * @return String Content from file.
		 */
		public function get($path){
			return file_get_contents($path);
		}

		/**
		 * 	This function checks if the files exists,
		 * @param  string $path the folder we're trying to find
		 * @return boolean      Does it exist or not?
		 */
		public function exist($path){
			return file_exists($path);
		}

		/**
		 * 	Scan the directory for anything in it.
		 * @param  string $path The folder we're trying to scan.
		 * @return array        An array with files that we found in the directory.
		 */
		public function scanDir($path){
			if(!$this->exist($path)){
				exit('THE FILE: ' . $path . 'DOES NOT EXIST!');
			}

			$var = scandir($path);
			$var = array_diff($var, array('.','..'));
			$var = array_values($var);
			return $var;
		}
	}
?>