<?php
	/**
     * name       user 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */  
	class _user {

		public function __construct(){

		}

		/**
		 * Login the user.
		 * @param  session       &$session       Session helper class.
		 * @param  core_database &$core_database Core database class.
		 * @param  String        $username       The username
		 * @param  String        $password       the password
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function loginUser(session &$session, core_database &$core_database, $username, $password){
			$account = $core_database->get("SELECT * FROM `users` WHERE `username`='".$username."'");

			if ( $username == $account['username'] &&  $this->checkPassword($password, $account['password'])){
				$salt = $this->setSalt($core_database, $account['username'], $account['password']);
				$this->setUserSession(	$session, $username, $salt);
				echo '<meta http-equiv="refresh" content="0; URL=http://'.$_SERVER['SERVER_NAME'].'/">';
			}
			return 0;
		}

		private function setSalt($core_database, $username, $password){
			$rand_string = mt_rand(0, 61);
			$salt = hash('sha512', $password . ($password . $rand_string) . $rand_string);
			$core_database->update('users',
										array('salt' => $salt),
										'WHERE `username`="' . $username . '"'
									);
			return $salt;
		}

		public function checkPassword($password, $db_password){
    		return (hash('sha256', $password) == $db_password) ? true : false;
		}
		/**
		 * Set the user session
		 * @param session &$session Session helper class
		 * 
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function setUserSession(session &$session, $username, $salt){
			$session->setSessionMultiple(
											array(
												"username",
												"fingerprint"
											),
											array(
												$username,
												password_hash($_SERVER['HTTP_USER_AGENT'] . $salt . $_SERVER['REMOTE_ADDR'], PASSWORD_BCRYPT)
											)
										);
			return 0;
		}

		/**
		 * Does this iser exist
		 * @param  core_database &$core_database Core database class.
		 * @param  String        $username       The usename
		 * 
		 * @return boolean
		 */
		public function isUser(core_database &$core_database, $username){
			return ($core_database->get("SELECT * FROM `users` WHERE `username`='".$username."'") > 0);
		}

		/**
		 * Does this user exist
		 * @param  core_database &$core_database Core database class.
		 * @param  String        $username       The usename
		 * 
		 * @return boolean
		 */
		public function isValidUser(core_database &$core_database, $username){
			return $core_database->hasValue($core_database->get("SELECT * FROM `users` WHERE `username`='".$username."'"));
		}

		/**
		 * Is the user secure
		 * @param  string  $fingerprint The fingerprint we added.
		 * @return boolean              Is he save?
		 */
		public function isUserSecure(core_database &$core_database, $username, $fingerprint){
			$salt = $core_database->get("SELECT `salt` FROM `users` WHERE `username`='".$username."'");
			return password_verify($_SERVER['HTTP_USER_AGENT'] . 
									$salt['salt']. 
									$_SERVER['REMOTE_ADDR'], 
									$fingerprint);
		}

		/**
		 * Get the user his username
		 * @param  session &$session session helper class.
		 * @return String            User his username
		 */
		public function getUserName(session &$session){
			return $session->getSession('username');
		}

		/**
		 * Is the user loged in?
		 * @param  session &$session session helper class.
		 * @return boolean
		 */
		public function isUserLogedIn(core_database &$core_database, session &$session){
			$username 		= $this->getUserName( $session );
			$fingerprint 	= $session->getSession('fingerprint');

			return ( null !== $fingerprint && $this->isUserSecure($core_database, $username, $fingerprint)) ? true : false;
		}

	}
?>