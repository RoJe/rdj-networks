<?php
	/**
     * name       dropdown 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */
	class _dropdown {
		public function __construct(){

		}

		public function getMenuItems($array){
			foreach($array as $index=>$link){
				foreach($link as $k=>$v){
					if(isset($v["title"]) && !empty($v["title"]["text"])) :
						?>
							<details class="navbar-app-menu-section-details">
						<?php
					endif;
					if(isset($v["title"]) && empty($v["title"]["text"])) :
						?>
							<div class="navbar-app-menu-section">	
						<?php
					endif;
					foreach($v as $p=>$j){
						if($p === "title"){
							if( !isset($j["icon"]) || empty( $j["icon"]) || !isset($j["text"]) || empty( $j["text"])){
								continue;
							}
							?>
								<summary class="navbar-app-menu-section-title">
									<span class="material-icons navbar-app-menu-section-title-icon"><?php echo $j["icon"]; ?></span>
									<span class="navbar-app-menu-section-title-text"><?php echo strtoupper( $j["text"] ); ?></span>
								</summary>
							<?php
							continue;
						}
						if(!isset($j[0])){
							$this->setHTMLLink($j);
							continue;
						}
						foreach($j as $t=>$g){
							$this->setHTMLLink($g);
							continue;
						}
					}
					if(isset($v["title"]) && !empty($v["title"]["text"])) :
						?>
							</details>
						<?php
					endif;
					if(isset($v["title"]) && empty($v["title"]["text"])) :
						?>
							</div>	
						<?php
					endif;
				}
			}
		}

		private function setHTMLLink($v){
			?>
				<a 
					class="navbar-app-menu-link <?php echo $v["name"]; ?>"
					href="<?php echo $v["link"]; ?>">
						<span class="navbar-app-menu-link-icon material-icons">
							<?php 
								echo $v["icon"];
							?>
						</span>
						<span class="navbar-app-menu-link-text">
							<?php
								echo strtoupper( $v["name"] );
							?>
						</span>
				</a>
			<?php
		}
	}
?>