<?php
	/**
     * name       session
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */
	class _session{
		public function __construct(){
			session_start();
		}

		/**
		 * For security reasons we don't want this class to be cloned.
		 */
		private function __clone(){
		}

		/**
		 * Create a session
		 * @param string $sessionName  The name of the session
		 * @param string/int/bool/etc.. $sessionValue The value of the session
		 */
		public function setSession($sessionName = "", $sessionValue ){
			$_SESSION[$sessionName] = $sessionValue;
			return 0;
		}


		/**
		 * Set multiple sessions
		 * @param array $sessionNames  The names of the sessions
		 * @param array $sessionValues The values of teh session
		 */
		public function setSessionMultiple( array $sessionNames, array $sessionValues){
			foreach($sessionNames as $index=>$value ){
				$this->setSession($value, $sessionValues[$index]);
			}
			return 0;
		}
		public function getSession($sessionName){
			return (( isset($_SESSION) && isset($_SESSION[$sessionName]) ) ? $_SESSION[$sessionName] : false );
		}
		
		/**
		 * Close the session and redirect him back to the home page.
		 */
		public function closeSessions(){
			session_destroy();
			header("Location: http://".$_SERVER['SERVER_NAME']);
			return 0;
		}
	}
?>