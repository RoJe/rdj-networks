<?php
	/**
     * name       string 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */  
	class _string{
		public function __construct(){
		}
		/**
		 * Make sure the string is save and only contains letters and numbers.
		 */
		public function getSafeString($string){	
			return preg_replace(array('/[a-zA-Z0-9]+$/'), array(''), $string); 
		}
	}
?>