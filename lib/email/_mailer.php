<?php
	/**
     * name       mailer 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */  
	class _mailer{
		public function __construct(){

		}

		/**
		 * Send an email.
		 * @param  String $to      The guy we send it to.
		 * @param  String $from    Our email
		 * @param  String $subject Subject
		 * @param  String $message The message we want to send
		 * 
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function sendMail( $to, $from, $subject, $message ){
			$from = "From: " . $from;
			mail($to, $subject, $message, $from);
			return 0;
		}	

	}
?>