<?php
	class _xml{
		public function __construct(){

		}

		/**
		 * Turn XML into an array.
		 * @param  xml $xml    The xml we want converted.
		 * @return Array
		 */
		public function xmlToObject($xml){
			$xml 		= simplexml_load_string($xml);
			$xml 		= json_encode($xml);
			$xml 		= json_decode($xml);
			return $xml;
		}
	}
?>