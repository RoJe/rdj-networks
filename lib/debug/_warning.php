<?php
	/**
     * name       warning 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */ 
	class _warning {
		public function __construct(){

		}
		/**
		 * Print out the default error message.
		 * @param  String $message The error message
		 * 
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function defaultErrorMessage($message){
			prinf('<h1 class="error-message-default">%s</h1>', $message);
			return 0;
		}

		/**
		 * Print out the form error message.
		 * @param  String $message The error message
		 * 
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function formErrorMessage($message){
			printf('<div class="error-message-form"><span>%s</span></div>');
			return 0;
		}

		/**
		 * Throw out an exeption.
		 * @param  String $message The error message
		 * 
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function exeption($message){
			throw new exeption($message);
			return 0;
		}

	}
?>
