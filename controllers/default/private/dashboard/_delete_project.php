<?php
	class _delete_project{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_helper_functions &$core_helper_functions, core_config &$core_config, core_database &$core_database, core_content &$core_content){
			$this->core_helper_functions = $core_helper_functions;
			$this->core_config 			 = $core_config;
			$this->core_database 		 = $core_database;
			$this->core_content 		 = $core_content;



			$this->user_helper 			 = $core_helper_functions->getHelperFunctions('user');
			$this->session_helper		 = $core_helper_functions->getHelperFunctions('session');
			$this->string_helper		 = $core_helper_functions->getHelperFunctions('string');

		}
		public function getProjectList(){
			$var = $this->core_database->getMultiple("SELECT * FROM `projects` WHERE `id_creator`='" . $this->user_helper->getUserName($this->session_helper) . "'");
		  	foreach($var as $index=>$value){
		  		echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
		  	}
		}
		public function doDeleteDeclarations($table, $args){
			if(!$this->string_helper->isMysqlSaveStringMultiple($args)){
				$this->printErrorMessage('INVALID CHARACTERS USED. ABORTING.');
				return 0;
			}
			if(!$this->isValidProject($args)){
				$this->printErrorMessage('INVALID PROJECT SELECTED. ABORTING.');
				return 0;
			}
			$this->core_database->delete($table, $args);
			$this->refresh();
			return 0;
		}
		public function isValidProject($args){
			return($this->core_database->get('SELECT * FROM `projects` WHERE `id`=' . $args['id']) > 0);
		}
		public function refresh(){
			echo '<meta http-equiv="refresh" content="0">';
		}
		public function printErrorMessage($message){
			echo '<h3 class="login-text-warning">'.$message.'</h3>';
		}
	}
?>