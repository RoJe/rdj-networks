<?php
	class _dashboard{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_helper_functions &$core_helper_functions, core_config &$core_config, core_database &$core_database, core_content &$core_content){
			$this->core_helper_functions = $core_helper_functions;
			$this->core_config 			 = $core_config;
			$this->core_database 		 = $core_database;
			$this->core_content 		 = $core_content;



			$this->user_helper 			 = $core_helper_functions->getHelperFunctions('user');
			$this->session_helper		 = $core_helper_functions->getHelperFunctions('session');
			$this->string_helper		 = $core_helper_functions->getHelperFunctions('string');
		}

		public function getRequestedPageVieuw(){
			return ((isset($_GET['subpage']) && $_GET['subpage'] != "") ? $_GET['subpage'] : null);
		}
	}
?>