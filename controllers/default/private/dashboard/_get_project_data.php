<?php
	class _get_project_data{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_helper_functions &$core_helper_functions, core_config &$core_config, core_database &$core_database, core_content &$core_content){
			$this->core_helper_functions = $core_helper_functions;
			$this->core_config 			 = $core_config;
			$this->core_database 		 = $core_database;
			$this->core_content 		 = $core_content;



			$this->user_helper 			 = $core_helper_functions->getHelperFunctions('user');
			$this->session_helper		 = $core_helper_functions->getHelperFunctions('session');
			$this->string_helper		 = $core_helper_functions->getHelperFunctions('string');

		}

		public function getProjectList(){
			$var = $this->core_database->getMultiple("SELECT * FROM `projects` WHERE `id_creator`='" . $this->user_helper->getUserName($this->session_helper) . "'");
		  	foreach($var as $index=>$value){
		  		echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
		  	}
		}

		public function doGetProjectData($project_id){
			$project 		= $this->core_database->get('SELECT * FROM `projects` WHERE `id_creator`="' .$this->user_helper->getUserName($this->session_helper). '" AND 
				`id` ='. $project_id);
			$contributors 	= $this->core_database->getMultiple('SELECT * FROM `users_has_projects` WHERE `projects_id`="' .$project_id. '" AND `accepted`=1');
			$declarations	= $this->core_database->getMultiple('SELECT * FROM `projects_has_declarations` WHERE `projects_id`="' .$project_id. '"');

			for ($count = 0; $count < 100; $count++){
				if ($count == 0){
					$text = '<tr><td>'.$project['name'].'</td><td>'.$this->user_helper->getUserName($this->session_helper).'</td>';
				}else{
					$text .= "<tr><td></td><td></td>";
				}
				if (isset($contributors[$count])){
					$contributor_user =  $this->core_database->get('SELECT `username` FROM `users` WHERE `usercode`="' .$contributors[$count]['usercode']. '"');
					$text .= "<td>" . $contributor_user['username'] . "</td>";
				}else{
					$text .= "<td></td>";
				}
				if(isset($declarations[$count])){
					$declaration_record =  $this->core_database->get('SELECT * FROM `declarations` WHERE `id`="' .$declarations[$count]['declarations_id']. '" AND `project_id`="' .$project_id. '"');
					$text .= '<td>'.$declaration_record['id'].'</td>';
					$text .= '<td>'.$declaration_record['name'].'</td>';
					$text .= '<td>'.$declaration_record['date'].'</td>';
					$text .= '<td>'.$declaration_record['cost'].'</td>';


				}else{
					$text .= "<td></td><td></td><td></td><td></td>";
				}

			}

			foreach($declarations as $index=>$declaration){
				$declaration_record =  $this->core_database->get('SELECT * FROM `declarations` WHERE `id`="' .$declaration['declarations_id']. '" AND `projects_id`="' .$project_id. '"');
				$text .= '<td></td>';
			}

			echo $text;


		}
}