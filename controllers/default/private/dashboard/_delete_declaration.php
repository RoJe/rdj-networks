<?php
	class _delete_declaration{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_helper_functions &$core_helper_functions, core_config &$core_config, core_database &$core_database, core_content &$core_content){
			$this->core_helper_functions = $core_helper_functions;
			$this->core_config 			 = $core_config;
			$this->core_database 		 = $core_database;
			$this->core_content 		 = $core_content;



			$this->user_helper 			 = $core_helper_functions->getHelperFunctions('user');
			$this->session_helper		 = $core_helper_functions->getHelperFunctions('session');
			$this->string_helper		 = $core_helper_functions->getHelperFunctions('string');

		}
		public function getDeclarationList(){
			$var = $this->core_database->getMultiple("SELECT * FROM `declarations` WHERE `users_username`='" . $this->user_helper->getUserName($this->session_helper) . "'");
		  	foreach($var as $index=>$value){
		  		echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
		  	}
		}
		public function doDeleteDeclarations($table, $args){
			if(!$this->string_helper->isMysqlSaveStringMultiple($args)){
				$this->printErrorMessage('INVALID CHARACTERS USED. ABORTING.');
				return 0;
			}
			if(!$this->isValidDeclaration($args)){
				$this->printErrorMessage('INVALID COST TYPE SELECTED. ABORTING.');
				return 0;
			}
			$this->core_database->delete($table, $args);
			$this->refresh();
			return 0;
		}
		public function isValidDeclaration($args){
			return($this->core_database->get('SELECT * FROM `declarations` WHERE `users_username`="' . $this->user_helper->getUserName($this->session_helper) . '" AND id='.$args['id']) > 0);
		}
		public function refresh(){
			echo '<meta http-equiv="refresh" content="0">';
		}
		public function printErrorMessage($message){
			echo '<h3 class="login-text-warning">'.$message.'</h3>';
		}
	}
?>