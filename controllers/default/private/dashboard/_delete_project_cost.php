<?php
	class _delete_project_cost{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_helper_functions &$core_helper_functions, core_config &$core_config, core_database &$core_database, core_content &$core_content){
			$this->core_helper_functions = $core_helper_functions;
			$this->core_config 			 = $core_config;
			$this->core_database 		 = $core_database;
			$this->core_content 		 = $core_content;



			$this->user_helper 			 = $core_helper_functions->getHelperFunctions('user');
			$this->session_helper		 = $core_helper_functions->getHelperFunctions('session');
			$this->string_helper		 = $core_helper_functions->getHelperFunctions('string');

		}
		public function doDeleteProjectCost($table, $args){
			if(!$this->string_helper->isMysqlSaveStringMultiple($args)){
				$this->printErrorMessage('INVALID CHARACTERS USED. ABORTING.');
				return 0;
			}
			if(!$this->isValidProject($args)){
				$this->printErrorMessage('INVALID PROJECT SELECTED. ABORTING.');
				return 0;
			}
			if(!$this->isValidDeclaration($args)){
				$this->printErrorMessage('INVALID DECLARATION SELECTED. ABORTING.');
				return 0;
			}
			$this->core_database->delete($table, $args);
			$this->refresh();
			return 0;
		}
		public function getProjectCostList(){
			$var = $this->core_database->getMultiple("SELECT DISTINCT `projects_id` FROM `projects_has_declarations` WHERE `declarations_users_username`='" . $this->user_helper->getUserName($this->session_helper) . "'");
		  	foreach($var as $index=>$value){
		  		$declName = $this->core_database->get('SELECT * FROM `projects` WHERE `id`=' . $value['projects_id']);
		  		echo '<option value="'.$value['projects_id'].'">'.$declName['name'].'</option>';
		  	}
		}
		public function getProjectDeclarationList(){
			$var = $this->core_database->getMultiple("SELECT * FROM `projects_has_declarations` WHERE `declarations_users_username`='" . $this->user_helper->getUserName($this->session_helper) . "' AND `projects_id`='" . $_GET['project'] . "'");
		  	foreach($var as $index=>$value){
		  		$declName = $this->core_database->get('SELECT * FROM `declarations` WHERE `id`=' . $value['declarations_id']);
		  		echo '<option value="'.$value['declarations_id'].'">'.$declName['name'].'</option>';
		  	}
		}
		public function isValidProject($args){
			return($this->core_database->get('SELECT * FROM `projects` WHERE `id`=' . $args['projects_id']) > 0);
		}
		public function isValidDeclaration($args){
			return($this->core_database->get('SELECT * FROM `declarations` WHERE `users_username`="' . $this->user_helper->getUserName($this->session_helper) . '" AND id='.$args['declarations_id']) > 0);
		}
		public function refresh(){
			echo '<meta http-equiv="refresh" content="0">';
		}
		public function printErrorMessage($message){
			echo '<h3 class="login-text-warning">'.$message.'</h3>';
		}
	}
?>