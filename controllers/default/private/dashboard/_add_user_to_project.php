<?php
	class _add_user_to_project{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_helper_functions &$core_helper_functions, core_config &$core_config, core_database &$core_database, core_content &$core_content){
			$this->core_helper_functions = $core_helper_functions;
			$this->core_config 			 = $core_config;
			$this->core_database 		 = $core_database;
			$this->core_content 		 = $core_content;



			$this->user_helper 			 = $core_helper_functions->getHelperFunctions('user');
			$this->session_helper		 = $core_helper_functions->getHelperFunctions('session');
			$this->string_helper		 = $core_helper_functions->getHelperFunctions('string');

		}

		public function doAddUserToProject($table, $args){
			if(!$this->string_helper->isMysqlSaveStringMultiple($args)){
				$this->printErrorMessage('INVALID CHARACTERS USED. ABORTING.');
				return 0;
			}
			if(!$this->isValidProject($args)){
				$this->printErrorMessage('INVALID PROJECT. ABORTING.');
				return 0;
			}
			if(!$this->isValidUserCode($args)){
				$this->printErrorMessage('INVALID USER CODE. ABORTING.');
				return 0;
			}
			$this->core_database->insert($table, $args);
			$this->refresh();
			return 0;
		}

		public function getProjectList(){
			$var = $this->core_database->getMultiple("SELECT * FROM `projects` WHERE `id_creator`='" . $this->user_helper->getUserName($this->session_helper) . "'");
		  	foreach($var as $index=>$value){
		  		echo '<option value="'.$value['id'].'">'.$value['name'].',  '.$value['discription'].'</option>';
		  	}
		}
		public function isValidProject($args){
			return($this->core_database->get('SELECT * FROM `projects` WHERE `id`=' . $args['projects_id']) > 0);
		}
		public function isValidUserCode($args){
			return ($this->core_database->get("SELECT * FROM `users` WHERE `usercode`='".$args['usercode']."'") > 0 );
		} 
		public function refresh(){
			echo '<meta http-equiv="refresh" content="0">';
		}
		public function printErrorMessage($message){
			echo '<h3 class="login-text-warning">'.$message.'</h3>';
		}
	}
?>