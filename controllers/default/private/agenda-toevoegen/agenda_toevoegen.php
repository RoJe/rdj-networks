<?php
	class agenda_toevoegen{
		private $core_library;
		private $core_database;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_system &$core_system){
			$this->core_library 		= $core_system->getCoreLibraryFunctions();
			$this->core_database 		= $core_system->getCoreDatabase();



			$this->user_helper 			= $this->core_library->getLibraryFunctions('user', 'user');
			$this->session_helper		= $this->core_library->getLibraryFunctions('session', 'session');
			$this->string_helper		= $this->core_library->getLibraryFunctions('string', 'string_filter');

		}

		public function getUserList(){
			$users 		= $this->core_database->getMultiple('SELECT `username` FROM `users`');
			$username 	= $this->user_helper->getUserName($this->session_helper);
			foreach($users as $key=>$value){
				if($value['username'] == $username){
					continue;
				}
				print_r('<option value="'.$value['username'].'">');
			}
		}

		public function getActivityTypes(){
			$users 		= $this->core_database->getMultiple('SELECT `name`, `description` FROM `activity_types`');
			foreach($users as $key=>$value){
				print_r('<option value="'.$value['name'].'">'.$value['description'].'</option>}
				option');
			}
		}

		public function doAddAgendaPoint($name, $date, $activity, $priority, $from, $to, $users, $description){
			$user_id 		= $this->core_database->get('SELECT `id` FROM `users` WHERE `username`="'.$this->user_helper->getUserName($this->session_helper).'"');
			$activity_id 	= $this->core_database->get('SELECT `id` FROM `activity_types` WHERE `name`="'.$activity.'"');
			$project_id 	= $this->core_database->insert('activities',
											array(
													'activity_types_id' => $activity_id['id'],
													'name'				=> $name,
													'description' 		=> $description,
													'date_added'		=> date('Y-m-d'),
													'date_start'		=> $date,
													'time_start'		=> $date . ' ' . $from . ':00',
													'time_end'			=> $date . ' ' . $from . ':00',
													'priority'			=> $priority,
													'finished'			=> 0
												)
										);
			$this->core_database->insert('activities_has_users',
											array(
													'activities_id' => $project_id,
													'users_id' 		=> $user_id['id'],
													'approved' 		=> 1
												)
										);


		}

}
		
?>