<?php
	class _promotion_location_block{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;


		public function __construct(core_helper_functions &$core_helper_functions, core_config &$core_config, core_database &$core_database, core_content &$core_content){
			$this->core_helper_functions = $core_helper_functions;
			$this->core_config 			 = $core_config;
			$this->core_database 		 = $core_database;
			$this->core_content 		 = $core_content;
		}
	}
?>