<?php
	class _login{
		private $core_library;
		private $core_database;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_system &$core_system){
			$this->core_library 		= $core_system->getCoreLibraryFunctions();
			$this->core_database 		= $core_system->getCoreDatabase();



			$this->user_helper 			= $this->core_library->getLibraryFunctions('user', 'user');
			$this->session_helper		= $this->core_library->getLibraryFunctions('session', 'session');
			$this->string_helper		= $this->core_library->getLibraryFunctions('string', 'string_filter');

		}
		public function doLogin($username, $password){
			if(!$this->string_helper->isMysqlSaveStringMultiple(array($username, $password))){
				$this->printErrorMessage('INCORRECT USERNAME OR PASSWORD, PLEASE TRY AGAIN. ');
				return;
			}
			if (!$this->user_helper->isUser($this->core_database, $username)){
				$this->printErrorMessage('THIS USER DOES NOT EXSIST. YOU CAN REGISTER <a href="/register/">HERE</a>');
				return;
			}
			$this->user_helper->loginUser($this->session_helper,$this->core_database, $username, $password);
		}
		public function printErrorMessage($message){
			echo '<h3 class="login-text-warning">'.$message.'</h3>';
		}
	}
?>