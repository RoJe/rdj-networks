<?php
	class _calendar{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;

		private $dayLabels = array("M","D","W","D","V","Z","Z");
		private $monthLabel = array(
										"Jan" 	=> "Januari",
										"Feb" 	=> "Februari",
										"Mar" 	=> "Maart",
										"Apr" 	=> "April",
										"May" 	=> "Mei",
										"Jun" 	=> "Juni",
										"Jul"	=> "Juli",
										"Aug"	=> "Augustes",
										"Sep"	=> "September",
										"Oct"	=> "Oktober",
										"Nov"	=> "November",
										"Dec"	=> "December"
									);
     
	    private $currentYear=0;
	    private $currentMonth=0;
	    private $currentDay=0;
	    private $currentDate=null;
	    private $daysInMonth=0;
	    private $naviHref= null;


	    private $year;
	    private $month;
		public function __construct(core_system &$core_system){

			$this->year  = null;
	        $this->month = null;
		}

		public function show(){
			$this->year 		= (!isset($_GET['y']) ? date("Y",time()) : $_GET['y']);     
	        $this->month 		= (!isset($_GET['m']) ? date("m",time()) : $_GET['m']);

			$this->currentYear	= $this->year;
			$this->currentMonth	= $this->month;
			$this->daysInMonth 	= $this->_daysInMonth($this->month,$this->year);  

	        $content='<div id="calendar">'.
	                        '<div class="box">'.
	                        $this->_createNavi().
	                        '</div>'.
	                        '<div class="box-content">'.
	                                '<ul class="label">'.$this->_createLabels().'</ul>';   
	                                $content.='<div class="clear"></div>';     
	                                $content.='<ul class="dates">';    
	                                 
	                                $weeksInMonth = $this->_weeksInMonth();
	                                // Create weeks in a month
	                                for( $i=0; $i<$weeksInMonth; $i++ ){
	                                     
	                                    //Create days in a week
	                                    for($j=1;$j<=7;$j++){
	                                        $content.=$this->_showDay($i*7+$j);
	                                    }
	                                }
	                                 
	                                $content.='</ul>';
	                                 
	                                $content.='<div class="clear"></div>';     
	             
	                        $content.='</div>';
	                 
	        $content.='</div>';
	        return $content;   
		}

		private function _showDay($cellNumber){         
        	$firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));

        	if(intval($cellNumber) == intval($firstDayOfTheWeek)){
			 
				$this->currentDay = 1;
			 
			}
        	
        	if(($firstDayOfTheWeek != 1 && $cellNumber < $firstDayOfTheWeek) ||  ( $this->currentDay > $this->daysInMonth )){
        		return '<li class="empty"></li>';
        	} 
	         
			if( $this->currentDay <= $this->daysInMonth ){

				$this->currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.($this->currentDay)));

				$cellContent = $this->currentDay;

				$this->currentDay++;   
	        }  
	         
	        return '<li id="'.$this->currentDate.'" class="'.( ( $this->currentDate == date("Y-m-d")) ? "today calendarButton" : "calendarButton" ) . '" >'.$cellContent.'</li>';
		}

		private function _createNavi(){
			$nextMonth 	= $this->currentMonth == 12 ? 1 : (int)$this->currentMonth + 1;
			$nextYear 	= $this->currentMonth == 12 ?	  (int)$this->currentYear  + 1 : $this->currentYear;
			$preMonth 	= $this->currentMonth == 1  ? 12: (int)$this->currentMonth - 1;
			$preYear 	= $this->currentMonth == 1  ?     (int)$this->currentYear  - 1 : $this->currentYear;


			$dispCurMoth= $this->monthLabel[date('M',strtotime($this->currentYear . '-' . $this->currentMonth))];
			return
	            '<div class="header">'.
	                '<a class="prev" href="/agenda-overzicht/'.sprintf('%02d',$preMonth).'/'.$preYear.'/">Vorige</a>'.
	                    '<span class="title">' . $dispCurMoth . ' / ' . $this->currentYear .'</span>'.
	                '<a class="next" href="/agenda-overzicht/'.sprintf("%02d", $nextMonth).'/'.$nextYear.'/">Volgende</a>'.
	            '</div>';
		}

		private function _createLabels(){
			$content='';

	        foreach($this->dayLabels as $index=>$label){
	             
	            $content.='<li class="'.($label==6?'end title':'start title').' title">'.$label.'</li>';
	 
	        }
	         
	        return $content;
		}

		private function _weeksInMonth(){
			if( !isset($this->year) ) {
            	$this->year =  date("Y",time()); 
	        }
	         
	        if(!isset($this->month)) {
	            $this->month = date("m",time());
	        }
	         
	        // find number of days in this month
	        $daysInMonths = $this->_daysInMonth($this->month,$this->year);
	         
	        $numOfweeks = ($daysInMonths%7==0?0:1) + intval($daysInMonths/7);
	         
	        $monthEndingDay= date('N',strtotime($this->year.'-'.$this->month.'-'.$daysInMonths));
	         
	        $monthStartDay = date('N',strtotime($this->year.'-'.$this->month.'-01'));
	         
	        if($monthEndingDay<$monthStartDay){
	             
	            $numOfweeks++;
	         
	        }
	         
	        return $numOfweeks;
		}

		private function _daysInMonth(){
			if(!isset($this->year))
            $this->year =  date("Y",time()); 
 
	        if(!isset($this->month))
	            $this->month = date("m",time());
	        return date('t',strtotime($this->year.'-'.$this->month.'-01'));
		}
	}
?>