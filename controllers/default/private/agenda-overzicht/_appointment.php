<?php
	class _appointment{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;

		private $user_helper;
		private $session_helper;

		private $year;
		private $month;

		public function __construct(core_system &$core_system){
			$this->user_helper 			= $core_system->getCoreLibraryFunctions()->getLibraryFunctions('user', 'user');
			$this->session_helper		= $core_system->getCoreLibraryFunctions()->getLibraryFunctions('session', 'session');
			$this->core_database 		= $core_system->getCoreDatabase();


			$this->year 				= (!isset($_GET['y']) ? date("Y",time()) : $_GET['y']);     
	        $this->month 				= (!isset($_GET['m']) ? date("m",time()) : $_GET['m']);

		}

		public function getPersonalAppointments(){
			$username 			= $this->user_helper->getUserName($this->session_helper);
			$user_id 			= $this->core_database->get('SELECT `id` FROM `users` WHERE `username` = "'.$username.'"');
			$appointments 		= $this->core_database->getMultiple('SELECT b.`name`, b.`description`, b.`date_added`,b.`date_start`, b.`time_start`, b.`time_end`, b.`priority`, b.`finished`
																 FROM ( `activities_has_users` a, `activities` b ) 
																 WHERE a.`users_id`="'.$user_id['id'].'" AND b.`id`=a.`activities_id` AND MONTH(b.`date_start`) = MONTH('.$this->year. $this->month  . '01)');
			$newAppointments 	= $this->resortAppointmenstFromArray($appointments);
			$this->appointmentTemplate($newAppointments);
		}

		private function appointmentTemplate($appointments){
			$numOfWeeks = $this->getWeeks();
			$count = 0;
			for($i=0; $i < $numOfWeeks; $i++) :
				
				?>
					<div class="date-week-container">
				<?php
					for($k=1; $k <= 7; $k++) :
						$count++;
						if($count > $this->_daysInMonth($this->month,$this->year)){
							break;
						}
						$curDate = date('Y-m-d',strtotime($this->year.'-'.$this->month.'-'.$count)); 
					?>
						<div class="date-day-combined-container <?php echo $curDate; ?>">
					<?php																																						
						$this->getAppointmentFromArray($appointments, $curDate, $count);
					?>
						</div>
					<?php
					endfor;
				?>
					</div>
				<?php
			endfor;
		}

		private function daysInWeek(){         
	        return date('N',strtotime($this->year.'-'.$this->month.'-01'));
		}

		private function resortAppointmenstFromArray($appointmenst){
			$newAppointments = array();
			foreach($appointmenst as $key=>$value){
				foreach($value as $k => $v){
					if ($k == 3){
						$newAppointments[$v][] = $value; 
					}
				}
			}
			return $newAppointments;
		}

		private function getAppointmentFromArray($appointments, $curDate, $count){
			$curDate 			= $this->year . '-' . $this->month . '-' . ( $count < 10 ? '0' . $count : $count);
			if(isset($appointments[$curDate])){

				return $this->filledAppointmentBlock($appointments[$curDate], $curDate, $count);

			}
			return $this->emptyAppointmentBlock($curDate, $count);
		}

		private function filledAppointmentBlock($appointment, $curDate, $count){
			?>
				<a class="date-day">
				<?php
					foreach($appointment as $key => $value ) :	
				?>		

					<div class="date-day-container-filled">
						<div class="date-event-container">
							<div class="day-event-details">
								<span class="date-day-priority-<?php echo $appointment[$key]['priority']; ?>"></span>
								<span class="date-day-name"><?php echo $appointment[$key]['name']; ?></span>
							</div>
							<span class="date-day-time-start-end">
									<span class="material-icons">&#xE192;</span>
									<?php 
										$timeStart 		= explode( ' ', $appointment[$key]['time_start'] )[1];
										$newTimeStart 	= explode( ':', $timeStart )[0] . ':' . explode( ':', $timeStart )[1];
										$timeEnd   		= explode( ' ', $appointment[$key]['time_end'] )[1];
										$newTimeEnd 	= explode( ':', $timeEnd )[0] . ':' . explode( ':', $timeEnd )[1];

										echo $newTimeStart . ' - ' .  $newTimeEnd; 
									?>
								</span>
							<div class="day-event-description"><?php echo $value['description']; ?> </div>
							<a href="" class="day-event-button">Details</a>
						</div>
					</div>
						
				<?php
					endforeach;
					$this->emptyAppointmentBlock($curDate, $count);
				?>
				</a>
			<?php
		}

		private function emptyAppointmentBlock($curDate, $count){
			$count = ($count < 10 ? '0' . $count : $count);
			?>
				<a class="date-day-empty" href="/agenda-toevoegen/<?php echo $count; ?>/<?php echo $this->month; ?>/<?php echo $this->year; ?>/">
					<div class="date-day-container">
						<div class="date-event-container-empty">

							<div class="day-event-name"> 
								<span class="material-icons">&#xE916;</span> 
								<?php echo $curDate; ?>
								- Klik hier om een afspraak aan te maken.
							</div>
						</div>
					</div>
				</a>
			<?php
		}

		private function _daysInMonth(){
			if(!isset($this->year))
            $this->year =  date("Y",time()); 
 
	        if(!isset($this->month))
	            $this->month = date("m",time());
	        return date('t',strtotime($this->year.'-'.$this->month.'-01'));
		}

		private function getWeeks(){
			
	         
	        // find number of days in this month
	        $daysInMonths = (int)$this->_daysInMonth($this->month,$this->year);
	        $numOfweeks = floor( ($daysInMonths%7==0?0:1) + $daysInMonths/7 );


	        $monthEndingDay= date('N',strtotime($this->year.'-'.$this->month.'-'.$daysInMonths));
	         
	        $monthStartDay = date('N',strtotime($this->year.'-'.$this->month.'-01'));
	         
	        if($monthEndingDay<$monthStartDay){
	             
	            $numOfweeks++;
	         
	        }
	        return $numOfweeks;
		}
	}
?>