<?php
	class _login{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_helper_functions &$core_helper_functions, core_config &$core_config, core_database &$core_database, core_content &$core_content){
			$this->core_helper_functions = $core_helper_functions;
			$this->core_config 			 = $core_config;
			$this->core_database 		 = $core_database;
			$this->core_content 		 = $core_content;



			$this->user_helper 			 = $core_helper_functions->getHelperFunctions('user');
			$this->session_helper		 = $core_helper_functions->getHelperFunctions('session');
			$this->string_helper		 = $core_helper_functions->getHelperFunctions('string');

		}
		public function doLogin($username, $password){
			if(!$this->string_helper->isMysqlSaveStringMultiple(array($username, $password))){
				$this->printErrorMessage('INCORRECT USERNAME OR PASSWORD, PLEASE TRY AGAIN. ');
				return;
			}
			if (!$this->user_helper->isUser($this->core_database, $username)){
				$this->printErrorMessage('THIS USER DOES NOT EXSIST. YOU CAN REGISTER <a href="/register/">HERE</a>');
				return;
			}
			$this->user_helper->loginUser($this->session_helper,$this->core_database, $username, $password);
		}
		public function printErrorMessage($message){
			echo '<h3 class="login-text-warning">'.$message.'</h3>';
		}
	}
?>