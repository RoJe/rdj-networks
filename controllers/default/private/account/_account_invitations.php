<?php
	class _account_invitations{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_helper_functions &$core_helper_functions, core_config &$core_config, core_database &$core_database, core_content &$core_content){
			$this->core_helper_functions = $core_helper_functions;
			$this->core_config 			 = $core_config;
			$this->core_database 		 = $core_database;
			$this->core_content 		 = $core_content;



			$this->user_helper 			 = $core_helper_functions->getHelperFunctions('user');
			$this->session_helper		 = $core_helper_functions->getHelperFunctions('session');
			$this->string_helper		 = $core_helper_functions->getHelperFunctions('string');

		}

		public function doGetInvitations(){
			$usercode 	 = $this->core_database->get('SELECT `usercode` FROM `users` WHERE `username` = "'.$this->user_helper->getUserName($this->session_helper).'"');
			$invitations = $this->core_database->getMultiple('SELECT * FROM `users_has_projects` WHERE `usercode` = "'.$usercode['usercode'].'" AND accepted=0');

			foreach($invitations as $key => $invitation){
				$project = $this->core_database->get('SELECT * FROM `projects` WHERE `id`='.$invitation['projects_id']);
				print_r('<form action="" method="post" class="account-invite-form">
							<span class="account-invite-form-text">'.$project['name'].'</span>
							<span class="account-invite-form-text">'.$project['discription'].'</span>
							<input type="hidden" value="'.$invitation['projects_id'].'" name="project_id">
							<input type="submit" class="account-invite-form-button" name="action" value="+" />
							<input type="submit" class="account-invite-form-button" name="action" value="-" />

						</form>');
			}
		}

		public function doUpdateInvitation($id, $type){
			$usercode 	 = $this->core_database->get('SELECT `usercode` FROM `users` WHERE `username` = "'.$this->user_helper->getUserName($this->session_helper).'"');
			$accepted = ($type == "+" ? 1 : 0);
			if ( $accepted ){
				$this->core_database->update('users_has_projects', array(
																		"accepted" =>  $accepted
																	), 'WHERE `usercode`="' . $usercode['usercode'] . '" AND `projects_id` ='. $id );
				return;
			}
			$this->core_database->delete('users_has_projects', array(
																		'usercode' 		=> $usercode['usercode'],
																		'projects_id' 	=> $id
																	) );
		}
	}
?>