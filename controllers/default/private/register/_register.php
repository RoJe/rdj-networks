<?php
	class _register{
		private $core_helper_functions;
		private $core_config;
		private $core_database;
		private $core_content;


		private $user_helper;
		private $session_helper;
		private $string_helper;

		public function __construct(core_helper_functions &$core_helper_functions, core_config &$core_config, core_database &$core_database, core_content &$core_content){
			$this->core_helper_functions = $core_helper_functions;
			$this->core_config 			 = $core_config;
			$this->core_database 		 = $core_database;
			$this->core_content 		 = $core_content;



			$this->user_helper 			 = $core_helper_functions->getHelperFunctions('user');
			$this->session_helper		 = $core_helper_functions->getHelperFunctions('session');
			$this->string_helper		 = $core_helper_functions->getHelperFunctions('string');

		}
		public function doRegister($table, $dataFields){
			if(!$this->string_helper->isMysqlSaveStringMultiple($dataFields)){
				$this->printErrorMessage('INCORRECT USERNAME OR PASSWORD, PLEASE TRY AGAIN. ');
				return;
			}
			if ($this->user_helper->isUser($this->core_database, $dataFields["username"])){
				$this->printErrorMessage('THIS USER ALREADY EXISTS, PLEASE TRY ANOTHER');
				return;
			}
			$this->registerUser($table, $dataFields);
			$this->loginUser($dataFields["username"], $dataFields["password"]);
		}

		public function registerUser($table, $dataFields){
			$dataFields["password"] = password_hash($dataFields["password"], PASSWORD_BCRYPT);
			$this->core_database->insert($table, $dataFields);
			return 0;
		}
		public function loginUser( $username, $password ){
			$account = $this->core_database->get("SELECT * FROM `users` WHERE `username`='".$username."'");
			if ( $username == $account['username'] &&  password_verify( $password, $account['password'])){
					$this->user_helper->setUserSession(	$this->session_helper, $username );
					echo '<meta http-equiv="refresh" content="0;URL=http://'.$_SERVER['SERVER_NAME'].'/">';
			}
			return 0;
		}

		public function printErrorMessage($message){
			echo '<h3 class="login-text-warning">'.$message.'</h3>';
		}
	}
?>