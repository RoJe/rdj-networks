<?php
	/**
     * name       core_views 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */
	class views{

		/**
		 * The page we're on right now.
		 * @var String
		 */
		private $curPage;

		/**
		 * The current team defined in webpage.xml 
		 * @var String
		 */
		private $curSite;

		/**
		 * File helper class. Located at /helpers/file.php
		 * @var Obj
		 */
		private $file;


		/**
		 * An array filled with all the public top views.
		 * @var Array
		 */
		private $publicTopVieuws;

		/**
		 * An array filled with all the public bottom views.
		 * @var Array
		 */
		private $publicBottomViews;

		/**
		 * An array filled with page specific views.
		 * @var Array
		 */
		private $pageViews;

		public function __construct(system &$system){
			$this->file 			 	= &$system->getLib()->get('file', '_file');
			
			$this->curSite 				= $system->getIndex()->getSite();
			$this->curPage 				= $system->getIndex()->getPage();
		}

		/**
		 * include all the public top page views.
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function getPublicTopViews(){
			if ( !isset( $views ) ){
				$views = $this->file->scanDir('views/' . $this->curSite . '/public/top/');
			}
			foreach($views as $index=>$page){
				include_once('views/'.$this->curSite .'/public/top/'. $page );
			}
			
			return 0;
		}

		/**
		 * include all the public bottom page views.
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function getPublicBottomViews(){
			if ( !isset( $views ) ){
				$views = $this->file->scanDir('views/' . $this->curSite . '/public/bottom/');
			}

			foreach($views as $index=>$page){
				include_once('views/'.$this->curSite .'/public/bottom/'. $page );
			}
			return 0;
		}

		/**
		 * include all the page specific views.
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function getPageViews(){
			if ( !isset( $pages ) ){
				$pages = $this->file->scanDir('views/' . $this->curSite . '/private/' . $this->curPage . '/');
				natsort($pages);
			}

			foreach($pages as $index=>$page){
				include_once('views/' . $this->curSite . '/private/' . $this->curPage . '/' . $page );
			}

			return 0;
		}
	}
?>
