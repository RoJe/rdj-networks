<?php
	/**
     * name       system 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */
	class system{

		/**
		 * Core helper functions class.
		 * @var Obj
		 */
		private $lib;

		/**
		 * Core index controller class.
		 * @var Obj
		 */
		private $cIndex;

		/**
		 * Core config class.
		 * @var Obj
		 */
		private $config;

		/**
		 * Security Object
		 * @var Obj
		 */
		private $security;
		/**
		 * Core database class.
		 * @var Obj
		 */
		private $database;

		/**
		 * Core controllers class.
		 * @var Obj
		 */
		private $controllers;

		/**
		 * Core contect class.
		 * @var Obj
		 */
		private $content;

		/**
		 * Core views class.
		 * @var Obj
		 */
		private $views;

		/**
		 * Initialize all system related modules
		 */
		public function __construct(){
				$this->setLib();
				$this->setIndex($this);
				$this->setConfig($this);
				$this->setDatabase($this);
				$this->setController($this);
				$this->setContent($this);
				$this->setViews($this);
		}

		/**
		 * return the lib controller class.
		 * @return Obj
		 */
		public function &getLib(){
			return $this->lib;
		}

		/**
		 * Set the lib controller class.
		 * @param core_library &$core_library the lib controller
		 */
		private function setLib(){
			$this->lib = new lib();
			return 0;
		}
		/**
		 * Return the core index controller class.
		 * @return Obj
		 */
		public function &getIndex(){
			return $this->cIndex;
		}

		private function setIndex(system &$system){
			$this->cIndex	= new cIndex($system);
			return 0;
		}

		/**
		 * Return the core config controller class.
		 * @return Obj
		 */
		public function &getConfig(){
			return $this->config;
		}

		private function setConfig(system &$system){
			$this->config = new config($system);
			return 0;
		}

		/**
		 * Return the core database class.
		 * @return Obj
		 */
		public function &getDatabase(){
			return $this->database;
		}

		private function setDatabase(system &$system){
			$this->database = new database($system);
			return 0;
		}

		/**
		 * Return the core Controller class.
		 * @return Obj
		 */
		public function &getController(){
			return $this->controllers;
		}

		private function setController(system &$system){
			$this->controllers = new controllers($system);
			return 0;
		}
		/**
		 * Return the core content class.
		 * @return Obj
		 */
		public function &getContent(){
			return $this->content;
		}

		private function setContent(system &$system){
			$this->content = new content($system);
			return 0;
		}

		/**
		 * Return the core views class.
		 * @return Obj
		 */
		public function &getViews(){
			return $this->views;
		}

		private function setViews(system &$system){
			$this->views = new views($system);
		}
	}
?>