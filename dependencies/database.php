<?php
	/**
     * name       core_database 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */
	class database{
		/**
		 * File helper class. Located at /helpers/file.php
		 * @var Obj
		 */
		private $file;

		private $databaseConfig;

		/**
		 * The connection to the database
		 * @var Database connection
		 */
		private $conn; 

		/**
		 * Private thise because we don't need it.
		 */
		public function __construct(system &$system){
			$this->file 			 	= &$system->getLib()->get('file', '_file');
			$this->databaseConfig		= $system->getConfig()->getDatabaseConfig();
		}

		/**
		 * Set the connection to the database.
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		private function setConnection(){          
			$this->conn                  = new PDO("mysql:host=".$this->databaseConfig->host.";dbname=".$this->databaseConfig->databaseName, $this->databaseConfig->username, $this->databaseConfig->password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return 0;
		}
		/**
		 * Clode the database connection
		 * 
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		private function close(){
			$this->conn = null;  
			return 0;  
		}
		/**
		 * Set data in the database
		 * @param string $query The query string
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function set($query){    
			$this->setConnection(); 
			try{
				$this->conn->exec($query);
			}catch(PDOException $e){
				throw new exception($e->getMessage());
			}
			$last_id = $this->conn->lastInsertId();
			$this->close();
			return $last_id;
		}
		/**
		 * Get data from the database
		 * @param  String $query 	Query string
		 * @return Array        	The query result
		 */
		public function get($query){
			$this->setConnection();
			$stmt         	=  $this->conn->prepare($query);
			$stmt->execute();
			$result 		= $stmt->fetch(PDO::FETCH_ASSOC); 
			$this->close();
			return  $result;
		}
		/**
		 * Get data from the database
		 * @param  String $query 	Query string
		 * @return Array        	The query result
		 */
		public function getMultiple($query){
			$this->setConnection();
			$stmt         	=  $this->conn->prepare($query);
			$stmt->execute();
			$result 		= $stmt->fetchAll(); 
			$this->close();
			return  $result;
		}
		/**
		 * Update stuff in database
		 * @param  String 	$table      	What table you want to target
		 * @param  Array 	$tableField 	What fields you want to target
		 * @param  Array 	$dataFields 	What fields you want to set
		 * 
		 *
		 * @return int       				php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                           		things up.
		 */
		public function insert($table, $dataFields){
			$sql = "INSERT INTO " . $table . " ( `";
			$i = 0;
			$leng = count($dataFields);
			foreach( $dataFields as $fieldName=>$fieldData ){
				$i++;
				if($i == $leng ){
					$sql .= $fieldName . "` ) VALUES ( '";
					break;
				}
				$sql .= $fieldName . "`, `";
			}

			$i = 0;
			$leng = count($dataFields);
			foreach( $dataFields as $fieldName=>$fieldData ){
				$i++;
				if($i == $leng ){
					$sql .= $fieldData . "' ) ";
					break;
				}
				$sql .= $fieldData . "', '";
			}
			return $this->set($sql);
		}
		/**
		 * Update stuff in database
		 * @param  String 	$table  	Table we want to target
		 * @param  Array 	$values 	Array with values we want to update; Example: array( "firstname" => "john" )
		 * @param  String 	$where  	Does it need to be targeted? Example: "WHERE `firstname`='john' AND ( ... )" 
		 * 
		 * @return int       			php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                          	things up.
		 */
		public function update($table, $values, $where){
			$sql = "UPDATE " . $table . " SET ";
			$i = 0;
			$leng = count($values);
			foreach( $values as $fieldName=>$fieldData ){
				$i++;
				if($i == $leng ){
					$sql .= $fieldName . "='" . $fieldData. "' ";
					break;
				}
				$sql .= $fieldName . "='" . $fieldData. "', ";
			}
			if (!empty($where)){
				$sql .= " " . $where;
			}
			$this->set($sql);
			return 0;
		}
		/**
		 * Delete stuff in database
		 * @param  String 	$table  	table we want to target
		 * @param  Array 	$values 	array with values to target the row; Example: array( "firstname" => "john" )" 
		 * 
		 * @return int       			php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                          	things up.
		 */
		public function delete($table, $values){
			$sql = "DELETE  FROM " . $table . " WHERE ";
			$i = 0;
			$leng = count($values);
			foreach( $values as $fieldName=>$fieldData ){
				$i++;
				if($i == $leng ){
					$sql .= $fieldName . "='" . $fieldData. "' ";
					break;
				}
				$sql .= $fieldName . "='" . $fieldData. "' AND ";
			}
			$this->set($sql);
			return 0;
		}
		/**
		 * Does the mysql result actually contain anything?
		 * @param  Array  	$var  $result
		 * @return boolean       
		 */
		public function hasValue($var){
			return $var > 0;
		}
	}

?>