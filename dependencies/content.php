<?php
	/**
     * name       core_content 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */
	class content{
		/**
		 * File helper class. Located in /helpers/file.php
		 * @var Obj
		 */
		private $file;

		/**
		 * The current page we're on right now.
		 * @var String
		 */
		private $curPage;

		/**
		 * The current team defined in webpage.xml 
		 * @var String
		 */
		private $curSite;

		public function __construct(system &$system){
			$this->file 			 	= &$system->getLib()->get('file', '_file');
			
			$this->curSite 				= $system->getIndex()->getSite();
			$this->curPage 				= $system->getIndex()->getPage();
		}

		/**
		 * 	This function returns all the global css so it can be outputted in the head. 
		 * 	This is not a function you should change unless you really need to.
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up. 
		 */
		public function getPublicCss(){
			if ( !isset( $globalCss ) ){
				$globalCss = $this->file->scanDir('content/'.$this->curSite.'/public/css/');
			}
			foreach($globalCss as $index=>$file){
				echo '<link rel="stylesheet" type="text/css" href="/content/'.$this->curSite.'/public/css/'.$file.'">';
			}
			return 0;
		}

		/**
		 * 	This function returns all the global js so it can be outputted in the head. 
		 * 	This is not a function you should change unless you really need to.
		 * 	
		 * 	@return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up. 
		 */
		public function getPublicJs(){
			if ( !isset( $globalJs ) ){
				$globalJs = $this->file->scanDir('content/'.$this->curSite.'/public/js/');
			}
			foreach($globalJs as $index=>$file){
				echo '<script src="/content/'.$this->curSite.'/public/js/'.$file.'"></script>';
			}
			return 0;
		}

		/**
		 * 	This function sets all the global media in '/content/<cur theme>/public/media/'. 
		 * 	This is not a function you should change unless you really need to.
		 * 	
		 * 	@return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up. 
		 */
		public function getPublicMedia($img){
			if( !$this->file->exist( 'content/'.$this->curSite.'/public/media/' . $img ) ){
				return;
			}
			echo '<img src="content/'.$this->curSite.'/public/media/' . $img.'" alt="">';
		}

		/**
		 * This function returns all the page specific css in '/content/<cur theme>/private/<page name>/css/' so it can be outputted in the head.
		 * This is not a function you should change unless you really need to. 
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function getPageCss(){
			if ( !isset( $css ) ){
				$css = $this->file->scanDir('content/' . $this->curSite .'/private/' . $this->curPage . '/css/');
			}
			foreach($css as $index=>$pageStyle){
				echo  '<link rel="stylesheet" type="text/css" href="/content/'.$this->curSite.'/private/'.$this->curPage.'/css/'. $pageStyle .'">';
			}
			return 0;
		}

		/**
		 * This function returns all the page specific js in '/content/<cur theme>/private/<page name>/js/' so it can be outputted in the head.
		 * This is not a function you should change unless you really need to. 
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function getPageJs(){
			if ( !isset( $js ) ){
				$js = $this->file->scanDir('content/'.$this->curSite.'/private/' .$this->curPage. '/js/');
			}
			foreach($js as $index=>$pageJs){
				echo '<script src="/content/'.$this->curSite.'/private/'.$this->curPage.'/js/'.$pageJs.'" ></script>';
			}
			return 0;
		}

		/**
		 * 	This function sets all the page specific media in '/content/<cur theme>/private/<page name>/media/'.
		 * 	This is not a function you should change unless you really need to.
		 *
		 *  @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up. 
		 */
		private function getPageMedia($img){
			if(!$this->file->exist('content/'.$this->curSite.'/private/'.$this->curPage.'/media/' . $img)){
				return;
			}
			echo '<img src="content/' . $this->curSite . '/public/media/' . $img . '" alt="">';
			return 0;
		}

	}
?>