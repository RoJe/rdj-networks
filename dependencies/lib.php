<?php
	/**
     * name       core_helper_functions 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */
	class lib{
		/**
		 * An array filled with all the file names of the lib folder. Located at /library/
		 * @var Array
		 */
		private $lib;


		public function __construct(){
		}

		/**
		 * Return the helper class or all of them.
		 * @param  String $className The name of the class.
		 * @return Array/Obj            
		 */
		public function &get($libname, $className){
			if(!isset($this->lib[$libname]) && !isset($this->lib[$libname][$className]) && !class_exists($className)){
				if(!$this->exist('lib/' . $libname . '/')){
					die('No file could be included. Check if directory exsists or if the file is missing.');
				}
				include_once( 'lib/' . $libname . '/' .  $className . '.php' );
				
				$this->lib[$libname][$className] = new $className();
			}
			return $this->lib[$libname][$className];
		}


		/**
		 * 	This function checks if the files exists,
		 * @param  string $path the folder we're trying to find
		 * @return boolean      Does it exist or not?
		 */
		private function exist($path){
			return file_exists($path);
		}
	}

?>