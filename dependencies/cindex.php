<?php
	/**
     * name       core_index_controller 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */
	class cIndex{
		private $system;
		private $file;
		private $string;

		public function __construct(system &$system){
			$this->system 	= &$system; 
			$this->file 	= &$system->getLib()->get('file', '_file');
			$this->string 	= &$system->getLib()->get('string', '_string');
		}

		public function getPage(){
			return(( isset($_GET['page']) && $this->file->exist('pages/' . $this->getSite() . '/' . $_GET['page'] . '.xml'))? $_GET['page'] : $this->system->getConfig()->getConfig()->default_page);
		}

		public function getSite(){
			return (isset($_GET['subsite']) ? $_GET['subsite'] : $this->system->getConfig()->getConfig()->primary_site);
		}
	}
?>