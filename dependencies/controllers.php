<?php
	/**
     * name       core_controllers 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */
	class controllers {
		/**
		 * 	The core system module
		 * @var Obj
		 */
		private $system;

		/**
		 * File helper class. Located at /helpers/file.php
		 * @var Obj
		 */
		private $file;

		/**
		 * The page we're on right now.
		 * @var String
		 */
		private $curPage;

		/**
		 * The current team defined in webpage.xml 
		 * @var String
		 */
		private $curSite;

		
		/**
		 * Array with all the public controllers. Located at /controllers/public/
		 * @var Array
		 */
		private $publicControllers;

		/**
		 * Array with all the controllers related to the page. Located at /controllers/private/<pagename>/
		 * @var Array
		 */
		private $privateControllers;


		public function __construct(system &$stystem){
			$this->system 					= &$stystem;
			$this->file 			 		= &$stystem->getLib()->get('file', '_file');
			$this->curSite 			 		= $stystem->getIndex()->getSite(); 
			$this->curPage 				 	= $stystem->getIndex()->getPage();
			


			$this->setPublicControllers();
		}

		/**
		 * Set all the public page controllers. Located at /controllers/public/
		 *
		 * @return int       	php has a thing that it won't escape the function properly until the second rotation. This just speeds 
		 *                      things up.
		 */
		public function setPublicControllers(){
			$controllers = $this->file->scanDir('controllers/'.$this->curSite.'/public/');
			foreach($controllers as $index=>$controller){
				$className = str_replace('.php', '', $controller);

				$this->publicControllers[$className] = 'controllers/'.$this->curSite.'/public/'. $controller;
			}
			unset($controllers);
			return 0;
		}

		/**
		 * Return a specific public controller or all of them.
		 * @param  string $className Name of the public controller
		 * @return Obj/Array           
		 */
		public function &getPublicControllers($className){


			if(isset($this->publicControllers[$className]) && !class_exists($this->publicControllers[$className])){

				include_once ( $this->publicControllers[$className] );
				$this->publicControllers[$className] = new $className($this->core_system);


				return $this->publicControllers[$className];
			}
			return (isset($this->publicControllers[$className]) ? $this->publicControllers[$className] : null);
		}

		/**
		 * Return a specific controller or all of them.
		 * @param  string $className Class name
		 * @return Array/Obj
		 */
		public function &getController($className){
			if($this->file->exist('controllers/'.$this->curTheme.'/private/'.$this->curPage . '/' . $className . '.php') && !class_exists($this->pageControllers[$className])){
				include_once ( $this->pageControllers[$className] );
				$this->pageControllers[$className] = new $className($this->core_system);


				return $this->pageControllers[$className];
			}
			return (isset($this->pageControllers[$className]) ? $this->pageControllers[$className] : null);
		}
	}
?>