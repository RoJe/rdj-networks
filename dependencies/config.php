<?php
	/**
     * name       config 
     *
     * author     (Robbert Mattheus de Jel <robbertdejel@hotmail.com>)
     */
	class config{
		private $system;
		/**
		 * file helper object. Located in /helpers/file.php
		 * @var Obj
		 */
		private $file;

		private $xml;

		/**
		 * The configuration options of the website.
		 * @var Array
		 */
		private $systemConfig;

		/**
		 * The database configuration of the website.
		 * @var Array
		 */
		private $databaseConfig;

		public function __construct(system &$system){
			$this->system 		= &$system;
			$this->file			= &$system->getLib()->get('file', '_file');
			$this->xml			= &$system->getLib()->get('xml', '_xml');
		}

		/**
		 * Set the options for the website and turn them into an array for later use.
		 */
		public function &getConfig(){
			if(!isset($this->systemConfig)){
				$this->systemConfig = $this->xml->xmlToObject($this->file->get("config/system.xml"));
			}
			return $this->systemConfig;
		}

		/**
		 * Set the options for the database and turn them into an array for later use.
		 */
		public function &getDatabaseConfig(){
			if(!isset($this->databaseConfig)){
				$this->databaseConfig = $this->xml->xmlToObject($this->file->get("config/database.xml"));
			}
			return $this->databaseConfig;
		}
	}
?>